﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoRunFilesExplorer
{
    public class RegistryFiles
    {
        //[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run]
        //[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce]
        //[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunServices]
        //[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunServicesOnce]
        //[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\Userinit]

        //[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run]
        //[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\RunOnce]
        //[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\RunServices]
        //[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\RunServicesOnce]
        //[HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows]

        /// <summary>
        /// Get files list from all startup Registry keys.
        /// </summary>
        /// <param name="items">Object of the List<ORMFileExplorerItem> type</param>
        public void GetFiles(ref List<ORMFileExplorerItem> items)
        {
            GetRegFilesLocalMachine(@"Software\\Microsoft\\Windows\\CurrentVersion\\Run", ref items);
            GetRegFilesLocalMachine(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce", ref items);
            //GetRegFilesLocalMachine(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunServices", ref items);
            //GetRegFilesLocalMachine(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce", ref items);
            //GetRegFilesLocalMachine(@"Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\Userinit", ref items);

            GetRegFilesCurrentUser(@"Software\\Microsoft\\Windows\\CurrentVersion\\Run", ref items);
            GetRegFilesCurrentUser(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce", ref items);
            //GetRegFilesCurrentUser(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunServices", ref items);
            //GetRegFilesCurrentUser(@"Software\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce", ref items);
            //GetRegFilesCurrentUser(@"Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\Userinit", ref items);
        }

        /// <summary>
        /// Get foreach files from Registry.
        /// </summary>
        /// <param name="sRegistryPath">Example: "Software\\Microsoft\\Windows\\CurrentVersion\\Run"</param>
        protected void GetRegFilesLocalMachine(string sRegistryPath, ref List<ORMFileExplorerItem> items)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegistryPath, false);

            foreach (string appName in key.GetValueNames())
            {
                lock (items)
                {
                    var val = key.GetValue(appName).ToString();
                    items.Add(new ORMFileExplorerItem(val, FilesExplorer.FILE_TYPE_REGISTRY));
                }
            }
        }

        protected void GetRegFilesCurrentUser(string sRegistryPath, ref List<ORMFileExplorerItem> items)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(sRegistryPath, false);

            foreach (string appName in key.GetValueNames())
            {
                lock (items)
                {
                    var val = key.GetValue(appName).ToString();
                    items.Add(new ORMFileExplorerItem(val, FilesExplorer.FILE_TYPE_REGISTRY));
                }
            }
        }
    }
}

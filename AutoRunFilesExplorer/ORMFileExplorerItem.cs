﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows;

namespace AutoRunFilesExplorer
{
    public class ORMFileExplorerItem
    {
        public string FullPathNameParams { get; set; }

        public BitmapSource IconImage { get; set; }
        public string FileName { get; set; }
        public string FileParams { get; set; }
        public string FilePath { get; set; }
        public string FileRunType { get; set; }
        public string FileSigned { get; set; }
        public string FileSignedCorrect { get; set; }
        public string Manufacturer { get; set; }

        /// <summary>
        /// public constructor. 
        /// </summary>
        /// <param name="_fullPathNameParams">_fullPathNameParams = FullPathNameParams</param>
        public ORMFileExplorerItem(string _fullPathNameParams, string _fileRunType)
        {
            FullPathNameParams = _fullPathNameParams;
            FileRunType = _fileRunType;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoRunFilesExplorer
{
    public class IconFile
    {
        public static System.Drawing.Icon GetIcon(string filename)
        {
            try
            {
                // get default file icon
                System.Drawing.Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(filename);
                return icon;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows;
namespace AutoRunFilesExplorer
{
    /// <summary>
    /// Base abstract class for any files sources: Registry, Start Menu, Scheduler.
    /// </summary>
    public class FilesExplorer
    {
        /// <summary>
        /// Name of the "Registry" file type.
        /// </summary>
        public const string FILE_TYPE_REGISTRY = "Registry";
        /// <summary>
        /// Name of the "StartMenu" file type.
        /// </summary>
        public const string FILE_TYPE_STARTMENU = "StartMenu";

        public const string FILE_CERTIFICATE_VALID = "Yes";
        public const string FILE_CERTIFICATE_INVALID = "No";

        /// <summary>
        /// Fields for self class event.
        /// </summary>
        public event TickHandler Tick;
        public EventArgs e = null;
        public delegate void TickHandler(FilesExplorer m, EventArgs e);

        /// <summary>
        /// List of the ORM files items.
        /// </summary>
        protected List<ORMFileExplorerItem> Items = null;

        /// <summary>
        /// Threads list.
        /// </summary>
        protected List<Thread> Threads = new List<Thread>();

        /// <summary>
        /// Log real-time UI control.
        /// </summary>
        protected List<string> ListLog = new List<string>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lstLog">May be null</param>
        public FilesExplorer() { }

        /// <summary>
        /// Start Menu Files Explorer
        /// </summary>
        protected void RunStartMenuFilesExplorer()
        {
            //throw new NotImplementedException("RunStartMenuFilesExplorer");
            LogSave("Starting RunStartMenuFilesExplorer()");

            StartMenuFiles startMenuFiles = new StartMenuFiles();
            startMenuFiles.GetFiles(ref Items);

            LogSave("Stop RunStartMenuFilesExplorer()");
        }

        /// <summary>
        /// Registry Files Explorer
        /// </summary>
        protected void RunRegistryFilesExplorer()
        {
            //throw new NotImplementedException("RunRegistryFilesExplorer");
            LogSave("Starting RunRegistryFilesExplorer()");

            RegistryFiles registryFiles = new RegistryFiles();
            registryFiles.GetFiles(ref Items);

            LogSave("Stop RunRegistryFilesExplorer()");
        }

        /// <summary>
        /// Scheduler Files Explorer
        /// </summary>
        protected void RunSchedulerFilesExplorer()
        {
            //throw new NotImplementedException("RunSchedulerFilesExplorer");
            LogSave("Starting RunSchedulerFilesExplorer()");

            SchedulerFiles schedulerFiles = new SchedulerFiles();
            schedulerFiles.GetFiles(ref Items);

            LogSave("Stop RunSchedulerFilesExplorer()");
        }

        /// <summary>
        /// Add threadDelegate to the Threads collection.
        /// </summary>
        /// <param name="threadDelegate"></param>
        protected void AddThread(ThreadStart threadDelegate)
        {
            Thread thread = new Thread(threadDelegate);
            Threads.Add(thread);
            thread.Start();
        }

        /// <summary>
        /// Return True, if all Threads items are stoped.
        /// </summary>
        /// <returns></returns>
        public bool IsStoped()
        {
            lock (Threads)
            {
                foreach (Thread thread in Threads)
                {
                    if (thread.ThreadState != ThreadState.Stopped)
                        return false; // Found non-stoped Thread
                }
            }

            // All Threads items are stoped
            return true;
        }

        /// <summary>
        /// Add text record to ListLog.
        /// </summary>
        /// <param name="mess"></param>
        protected void LogSave(string mess)
        {
            if (ListLog != null)
            {
                string sDate = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss.ffff");
                lock (ListLog)
                {
                    ListLog.Add(sDate + ": " + mess);
                }
            }
        }

        /// <summary>
        /// Read-only property - get Log.
        /// </summary>
        public List<string> Log { get { return ListLog; } }

        /// <summary>
        /// Read-only property - files list.
        /// </summary>
        public List<ORMFileExplorerItem> Files { get { return Items; } }

        /// <summary>
        /// Check for all job Threads, and call ParseFilePathParams().
        /// </summary>
        public void WaitAndParseFilePathParams()
        {
            while (!IsStoped())
            {
                // waiting for finished job threads
                Thread.Sleep(500);
            }

            // Raise event
            Tick?.Invoke(this, e);
        }

        public static BitmapSource BitmapToBitmapSource(Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                          source.GetHbitmap(),
                          IntPtr.Zero,
                          Int32Rect.Empty,
                          BitmapSizeOptions.FromEmptyOptions());
        }

        /// <summary>
        /// Parse full file path+params to it's parts.
        /// </summary>
        public void ParseFilePathParams()
        {
            //throw new NotImplementedException("ParseFilePathParams");
            foreach (ORMFileExplorerItem item in Items)
            {
                string _fullPathNameParams = item.FullPathNameParams;

                // try find params
                string findParamsString = " /";
                int strpos = _fullPathNameParams.LastIndexOf(findParamsString);

                if (strpos >= 0) // found params
                {
                    item.FileParams = _fullPathNameParams.Substring(strpos + findParamsString.Length);
                    item.FilePath = _fullPathNameParams.Substring(0, strpos - 1);
                }
                else
                {
                    item.FileParams = "";
                    item.FilePath = _fullPathNameParams;
                }

                // try divide name from path
                string _fullFilePath = item.FilePath;

                findParamsString = @"\";
                strpos = _fullFilePath.LastIndexOf(findParamsString);

                if (strpos >= 0) // found path & name
                {
                    item.FileName = _fullFilePath.Substring(strpos + findParamsString.Length);
                    item.FilePath = _fullFilePath.Substring(0, strpos);
                }
                else // only name
                {
                    item.FileName = _fullFilePath;
                    item.FilePath = "";
                }

                // Remove quote (") char from begin
                item.FilePath = item.FilePath.Replace("\"", "");
                _fullFilePath = _fullFilePath.Replace("\"", "");

                // Check SignedFile
                SignedFile signedFile = new SignedFile(_fullFilePath);
                X509Certificate cert = signedFile.Cert;

                if (cert == null)
                {
                    // not signed file
                    item.FileSigned = "-";
                }
                else
                {
                    // file has X509Certificate signature
                    // get signature data
                    var cert2 = new X509Certificate2(cert);
                    string manufacturer = cert2.GetNameInfo(X509NameType.DnsName, false);
                    bool isValidCert = cert2.Verify();

                    item.FileSigned = cert.Subject;
                    item.Manufacturer = manufacturer;
                    item.FileSignedCorrect = isValidCert ? FILE_CERTIFICATE_VALID : FILE_CERTIFICATE_INVALID;
                }

                // get default file icon
                System.Drawing.Icon icon = IconFile.GetIcon(_fullFilePath);

                if (icon != null)
                    item.IconImage = BitmapToBitmapSource(icon.ToBitmap());
            }
        }

        /// <summary>
        /// Main method - get files data.
        /// </summary>
        public void Run()
        {
            Items = new List<ORMFileExplorerItem>();

            // Create threads delegates
            ThreadStart threadStartMenuDelegate = new ThreadStart(RunStartMenuFilesExplorer);
            ThreadStart threadRegistryDelegate = new ThreadStart(RunRegistryFilesExplorer);
            ThreadStart threadSchedulerDelegate = new ThreadStart(RunSchedulerFilesExplorer);

            // Run threads
            AddThread(threadStartMenuDelegate);
            AddThread(threadRegistryDelegate);
            AddThread(threadSchedulerDelegate);

            // Run waiting thread
            ThreadStart threadWaitDelegate = new ThreadStart(WaitAndParseFilePathParams);
            Thread threadWait = new Thread(threadWaitDelegate);
            threadWait.Start();
        }
    }
}

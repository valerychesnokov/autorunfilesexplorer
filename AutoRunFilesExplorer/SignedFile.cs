﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace AutoRunFilesExplorer
{
    public class SignedFile
    {
        protected X509Certificate m_cert = null;

        public SignedFile(string filepath)
        {
            m_cert = GetAppCertificate(filepath);
        }

        public X509Certificate Cert { get { return m_cert; } }

        /// <summary>
        /// Gets the certificate the file is signed with.
        /// </summary>
        /// <param name="filename">The path of the signed file from which to
        /// create the X.509 certificate. </param>
        /// <returns>The certificate the file is signed with</returns>
        protected X509Certificate GetAppCertificate(string filename)
        {
            try
            {
                X509Certificate cert = X509Certificate.CreateFromSignedFile(filename);
                return cert;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoRunFilesExplorer
{
    public class StartMenuFiles
    {
        /// <summary>
        /// Get files list from all startup Registry keys.
        /// </summary>
        /// <param name="items">Object of the List<ORMFileExplorerItem> type</param>
        public void GetFiles(ref List<ORMFileExplorerItem> items)
        {
            string sAllStartMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup);
            string sUserStartMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);

            AddFiles(sAllStartMenuPath, ref items);
            AddFiles(sUserStartMenuPath, ref items);
        }

        protected void AddFiles(string folderPath, ref List<ORMFileExplorerItem> items)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = System.IO.Directory.GetFiles(folderPath);
            foreach (string fileName in fileEntries)
            {
                items.Add(new ORMFileExplorerItem(fileName, FilesExplorer.FILE_TYPE_STARTMENU));
            }
        }
    }
}

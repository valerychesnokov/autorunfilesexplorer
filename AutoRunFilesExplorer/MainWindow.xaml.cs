﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoRunFilesExplorer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected FilesExplorer m_FilesExplorer = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Show About dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Valery V. Chesnokov\r\nhttp://chesnokov.FL34.ru\r\n+79272536805\r\n(RUS, UTC+3)"
                , "About"
                , MessageBoxButton.OK
                , MessageBoxImage.Information);
        }

        /// <summary>
        /// Run main method of the program task.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRun_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BtnRun.IsEnabled = false;

                //MessageBox.Show("BtnRun_Click");
                m_FilesExplorer = new FilesExplorer();

                // set event handler
                m_FilesExplorer.Tick += new FilesExplorer.TickHandler(TakeFilesExplorerEvent);

                // run job threads
                m_FilesExplorer.Run();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR"
                    , MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// FilesList ListView event handler: opens the folder in explorer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HandleDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // get folder path
                ORMFileExplorerItem item = ((ListViewItem)sender).Content as ORMFileExplorerItem;
                string path = item.FilePath;

                // opens the folder in explorer
                System.Diagnostics.Process.Start(@path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR"
                    , MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        /// <summary>
        /// FilesExplorer event handler.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="e"></param>
        private void TakeFilesExplorerEvent(FilesExplorer m, EventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, 
                new Action(() => {
                // call here
                ShowFilesToListView();
            }));
        }

        /// <summary>
        /// Show found files to ListView.
        /// </summary>
        private void ShowFilesToListView()
        {
            try
            {
                if (m_FilesExplorer == null)
                {
                    LblStatus.Content = "Not running";
                }
                else
                {
                    if (m_FilesExplorer.IsStoped())
                    {
                        m_FilesExplorer.ParseFilePathParams();

                        LstLog.ItemsSource = m_FilesExplorer.Log;
                        FilesList.ItemsSource = m_FilesExplorer.Files;

                        LblStatus.Content = "Stoped. Log items count: " + m_FilesExplorer.Log.Count.ToString();
                        BtnRun.IsEnabled = true;
                    }
                    else
                        LblStatus.Content = "Running now";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR"
                    , MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnStatus_Click(object sender, RoutedEventArgs e)
        {
            ShowFilesToListView();
        }
    }
}
